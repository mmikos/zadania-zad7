
Praca domowa #7
===============

Stworzyć we frameworku Django w pełni funkcjonalną aplikację do microblogowania dla wielu użytkowników.
Aplikacja powinna:

* korzystać z bazy danych (np. sqlite)
* wyświetlać najnowsze wpisy wszystkie oraz dla każdego użytkownika z osobna
* umożliwiać dodawanie wpisów dla zalogowanego użytkownika, a dla innego nie
* autentykować użytkowników (patrz: aplikacja ``django.contrib.auth`` i middleware ``django.contrib.auth.middleware.AuthenticationMiddleware``)
* wyświetlać informacje o błędach i sukcesach
* schludnie wyglądać
* działać w każdym przypadku

Funkcjonalnością odróżniającą tę aplikację od zeszłotygodniowej jest uwzględnienie większej liczby użytkowników
oraz odpowiadająca temu poprawna obsługa wyświetlania i dodawania wpisów uwzględniająca danego użytkownika.